package org.tw.battle;

import org.flywaydb.core.Flyway;
import org.tw.battle.domain.ServiceConfiguration;
import org.tw.battle.infrastructure.DatabaseConfiguration;
import org.tw.battle.infrastructure.DatabaseConnectionProvider;

import java.sql.Connection;

/**
 * @author Liu Xia
 */
public class Program {
    @SuppressWarnings("FieldCanBeLocal")
    private static Connection keepInMemoryConnection;

    public static void main(String[] args) throws Exception {
        final ServiceConfiguration configuration = new DatabaseConfiguration(
            "jdbc:h2:mem:prodDB;MODE=MYSQL;", "sa", "p@ssword", "org.h2.Driver"
        );

        keepInMemoryConnection = DatabaseConnectionProvider.createConnection(configuration);
        migrateDatabase(configuration);

        try {
            // Playing happily here.
        } finally {
            keepInMemoryConnection.close();
        }
    }

    private static void migrateDatabase(ServiceConfiguration configuration) {
        final Flyway migration = Flyway.configure().dataSource(
            configuration.getUri(),
            configuration.getUsername(),
            configuration.getPassword()
        ).load();
        migration.migrate();
    }
}
