CREATE TABLE IF NOT EXISTS `student` (
    `id`            INT             AUTO_INCREMENT PRIMARY KEY,
    `name`          VARCHAR(32)     NOT NULL
)